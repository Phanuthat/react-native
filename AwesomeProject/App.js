import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
});

class App extends React.Component {
  render() {
    return (
      <View style={styles.Container}>
        <View style={[styles.Header]}>
          <View style={[styles.box,styles.Textcenter]}>
            <Text>icon</Text>
          </View>
          <View style={[styles.box,styles.Textcenter]}>
            <Text>Text</Text>
          </View>
          <View style={[styles.box,styles.Textcenter]}>
            <Text>icon</Text>
          </View>
        </View>
        <View style={[styles.body, styles.Textcenter]}>
          <Text>Score</Text>
        </View>

        <View style={[styles.Header]}>
          <View style={[styles.box,styles.Textcenter]}>
            <Text>Text</Text>
          </View>
          <View style={[styles.box,styles.Textcenter]}>
            <Text>Text</Text>
          </View>
          <View style={[styles.box,styles.Textcenter]}>
            <Text>Text</Text>
          </View>
          <View style={[styles.box,styles.Textcenter]}>
            <Text>Text</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Container: {
    backgroundColor: "yellow",
    flex: 1
  },
  Header: {
    flexDirection: "row",
    marginHorizontal: -8
  },
  row: {
    flexDirection: "column",
    flex: 1
  },
  box: {
    backgroundColor: "red",
    flex: 1,
    margin: 8,
    padding: 16
  },
  box1: {
    backgroundColor: "red",
    flex: 2,
    margin: 8,
    padding: 16
  },

  body: {
    backgroundColor: "green",
    flex: 2
  },
  Textcenter: {
    alignItems: "center",
    justifyContent: "center"
  }
});
export default App;
