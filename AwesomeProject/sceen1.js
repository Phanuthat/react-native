import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import { red } from "ansi-colors";

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
});

class Sceen extends React.Component {
  render() {
    return (
      <View style={styles.Container}>
        <View style={styles.Row}>
          <View style={[styles.Cricle, styles.Center]}>
            <Text>Img</Text>
          </View>
          <View style={styles.Row}>
            <View style={[styles.Center, styles.Box]}>
              <Text>Input</Text>
            </View>
            <View style={[styles.Center, styles.Box]}>
              <Text>Input 2 </Text>
            </View>

            <View style={styles.Row}>
              <View style={[styles.Center, styles.Box1]}>
                <Text>Input 3 </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Container: {
    backgroundColor: "yellow",
    flex: 1
  },
  Cricle: {
    backgroundColor: "white",
    borderRadius: 100,
    margin: 100,

    flex: 1
  },
  Center: {
    alignItems: "center",
    justifyContent: "center"
  },
  Row: {
    flex: 1,
    flexDirection: "column"
  },
  Box: {
    // flex: 1,
    backgroundColor: "green",
    padding: 10,
    margin: 8
  },
  Box1: {
    // flex: 1,
    backgroundColor: "red",
    padding: 20,
    margin: 10
  }
});
export default Sceen;
